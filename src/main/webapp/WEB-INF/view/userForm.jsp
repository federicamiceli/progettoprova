<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<!DOCTYPE html >
    
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link type= "text/css"
	rel="stylesheet"
	href="${pageContext.request.contextPath}/WEB-INF/css/style.css">
	<title>New/Edit Contact</title>
	 <link rel="stylesheet"
		 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	
	<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div id="wrapper">
	   
	</div>
 	<div id="loginbox" style="margin-top: 50px;"
			class="mainbox col-md-8 col-md-offset-2 col-sm-15 col-sm-offset-20">
			
        <div class="panel panel-info">

				<div class="panel-heading">
					<div class="panel-title">Aggiungi Utente</div>
				</div>
	<div style="padding-top: 30px" class="panel-body">
		<div id="container">
		<form:form action="saveUser" modelAttribute="user" Method="POST">
		
		<div style="margin-bottom: 25px" class="input-group">
        	
        	           <label>Username</label>
		           <form:input type="email" path="username" class="form-control" required="true"/>
		       </div>
		       
		       
		         <div style="margin-bottom: 25px" class="input-group">
		            <label>Password</label>
		            <form:input type="password" path="pass" class="form-control" required="true"/>
		       </div> 
		       
		       
		         <div style="margin-bottom: 25px" class="input-group">
		            <label>Enabled</label>
		            <form:select path="enabled" class="form-control">
		            	<form:option value="1" label="True"></form:option>
				<form:option value="0" label="False"></form:option>
		            		            
		            </form:select>
		          </div>
		        
		        
		        <div style="margin-bottom: 25px" class="input-group">
		         <label>Authorities</label>
		        <form:select path="authorities" class="form-control">
					<form:option value="ROLE_EMPLOYEE" label="Employee"></form:option>
					<form:option value="ROLE_MANAGER" label="Manager"></form:option>
					<form:option value="ROLE_ADMIN" label="Admin"></form:option>
			</form:select>
			</div>
		           
		            <input type="submit" value="Save" class="btn btn-default"/>
		       		
		
		
		
		</form:form>
		<div style="clear; both;"></div>
		</div>
		</div>
    	</div>
		<a href="${pageContext.request.contextPath}/">Back to Home Page</a>
	
    	</div>
</body>
</html>