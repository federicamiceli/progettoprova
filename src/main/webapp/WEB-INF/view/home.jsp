<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
	<head>
		<title> Welcome to the home page</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	</head>

	<body>
		<h2>Welcome to the home page!!</h2>
		<br>
			
		
		<hr>
		
		<!-- stampo username e ruolo -->
		
		<p>
			<h3>Benvenuto in f2 Informatica:	 <security:authentication property="principal.username" /></h3>
			<br><br>
			Role(s): <security:authentication property="principal.authorities" />
			
		</p>
		
		<br>
		<div id="container">
		
			
		<iframe scrolling="no" frameborder="no" clocktype="html5" 
		style= "overflow:hidden; border:0; margin:0; padding:0; width:120px; height:40px;
		"src="https://www.clocklink.com/html5embed.php?clock=004&timezone=CET&color=black&size=120&Title=&Message=&Target=&From=2018,1,1,0,0,0&Color=black">
		</iframe>

		</div>

		<br><br>
		<form action="timbra" modelAttribute="user" class="form-horizontal" >
		<input type="submit" class="btn btn-success" value="Entrata"/>
						
		
		</form>
		<br><br>
		
		<!-- PERMETTE DI VEDERE LA SEZIONE SOLO SE CI SI LOGGA COME MANAGER -->
		
		<security:authorize access="hasRole('MANAGER')">
		<!-- Add to link per role leaders this is for the manager-->
		<p>
			<a href="${pageContext.request.contextPath}/leaders">Leadership Page</a>
					(Only for Manager peeps)
		</p>
		</security:authorize>
		
		<!-- PERMETTE DI VEDERE LA SEZIONE SOLO SE CI SI LOGGA COME ADMIN-->
		
		<security:authorize access="hasRole('ADMIN')">
		<!-- Add to link per role /system this is for the admin-->
		<p>
			<input type="button" class="btn btn-primary" value="Admin Page" 
				onClick="window.location.href='systems';" />
			  <input type="button" class="btn btn-primary" value="Aggiungi Utente"
		            	   onClick="window.location.href='showFormAdd'; return false;"
		            	   class="add-Button"
            	   		/>	
		</p>
		</security:authorize>
	
		<input type="button" class="btn btn-primary" value="Modifica Password"
		            	   onClick="window.location.href='modificaPass'; return false;"
		            	   class="add-Button"
            	   		/>	
	 		 
		
		<br><br>	
		
		
		<!-- Aggiungo bottone di logout -->
				
		<form:form action="${pageContext.request.contextPath}/logout"
				   method="POST">
				   
				<input type="submit"  class="btn btn-danger" value="Logout"/>
		</form:form>		   
				   
	</body>
</html>