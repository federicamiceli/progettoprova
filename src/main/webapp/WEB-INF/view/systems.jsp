<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>

	<head>

		<title>IT System Home Page</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
	</head>

	<body>
		<div id="wrapper">
			<div id="header">
		
				<h2>Admin Home Page</h2>
				<hr><hr>
				
			</div>
		</div>
		 <div align="center">
            <h2>User List</h2>
          
            <br>
            		<!-- lista utenti -->
            <table class="table table-sm">
            	<tr>
	                <th>Username</th>
	                <th>Password</th>
	                <th>Enable</th>
	              <!--   <th>Authorities</th> -->
	                <th>Action</th>
                 </tr>
                	<c:forEach var="tempUser" items="${listUser}" >
                	
                	<!--  construct an update link for User -->
                	
                	<c:url var="updateLink" value="/showFormUpdate">
                	   <c:param name="userName" value="${tempUser.username}" />
                	</c:url>
                	
                <tr>
                    
	                    <td>${tempUser.username}</td>
	                    <td><input type="password" value="${tempUser.pass}"/></td>
	                    <td>${tempUser.enabled}</td>
	                    
	                    <td>
	                    
	                    <!-- the update link -->
	                     <input type="button" class="btn btn-primary" value="Modifica Password"
		            	   onClick="window.location.href='modificaPass'; return false;"
		            	   class="add-Button"
            	   		/>	
	                         <input type="button" class="btn btn-primary" value="Elimina"
		            	   onClick="window.location.href='eliminaUser'; return false"
		            	   class="add-Button"
            	   		/>	
	                    </td>
	                             
	                </tr>
                	</c:forEach>             
            </table>
        </div>
		
		<a href="${pageContext.request.contextPath}/">Back to Home Page</a>
	
	</body>

</html>