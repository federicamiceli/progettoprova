<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
    <head>
    <title>Modifica Password</title>
    
    <link rel="stylesheet"
		 href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
	
	<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        
        
        
        <div id="loginbox" style="margin-top: 50px;"
			class="mainbox col-md-3 col-md-offset-2 col-sm-6 col-sm-offset-2">
			
        <div class="panel panel-info">

				<div class="panel-heading">
					<div class="panel-title">Modifica Password</div>
				</div>
        
        <div style="padding-top: 30px" class="panel-body">
        	
        	<form:form action="updatePass" modelAttribute="user" METHOD="POST" class="form-horizontal">
        	
        	<div style="margin-bottom: 25px" class="input-group">
        	
        	  <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span> 
        	  <input type="email" name="username" placeholder="Username" class="form-control"/>
        	   </div>
        	   
        	   
        	   <div style="margin-bottom: 25px" class="input-group">
			<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span> 
        	   <input type="password" name="pass" placeholder="Password" class="form-control" />
        	  </div>
        	  
        	  
        	   <div style="margin-top: 10px" class="form-group">						
							<div class="col-sm-6 controls">
        	   <button type="submit" value="Modifica" class="btn btn-default">Modifica</button> 
        	   </div>
        	</div>
        	</form:form>
        </div>
        </div>	
    	</div>
    	<a href="${pageContext.request.contextPath}/">Back to Home Page</a>
    	
    </body>
</html>