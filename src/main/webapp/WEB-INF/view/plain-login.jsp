<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<head>
		<title>Custom Login Page</title>
		
	<style>
		.failed{
			color: red;
		}
	</style>
	
	</head>


	<body>
		<h3> My Custom Login Page</h3>
			<form:form action="${pageContext.request.contextPath}/authenticateTheUser"
					method="POST">
					
				<!-- Chech invalid autentication -->
			
				<c:if test="${param.error !=null }">
					<i class="failed"> Sorry!! Username o Password non validi</i>
				</c:if>
			
				<p> 
				User name: <input type="text" name="username"/>
				</p>
		
				<p>  
				Password: <input type="password" name="password" />
				</p>
		
				<input type="submit" value="Login" />
		
			</form:form>
</body>

</html>