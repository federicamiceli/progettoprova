package it.f2Informatica.springsecurity.demo.entity;

public class User {
	private String username;
	private String pass;
	private Integer enabled;
	private String authorities;
	
	
	
	public User(String username, String pass) {
		this.username = username;
		this.pass = pass;
		
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}

	public Integer getEnabled() {
		return enabled;
	}
	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}
	
	
	public String getAuthorities() {
		return authorities;
	}
	public void setAuthorities(String authorities) {
		this.authorities = authorities;
	}
	public User() {
		super();

	}
	
	
	
	
	
	
	
	
	
}
