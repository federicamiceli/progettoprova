package it.f2Informatica.springsecurity.demo.entity;


import java.time.LocalDate;
import java.time.LocalTime;


public class Presenze {
	
	private int id;
	private String user;
	private LocalTime entrata1;
	private LocalTime uscita1;
	private LocalTime entrata2;
	private LocalTime uscita2;
	private String note;
	private LocalDate data;
	
	
	
	
	
	public Presenze() {
		super();
		
	}





	public Presenze(String user, LocalTime entrata1, LocalTime uscita1, LocalTime entrata2, LocalTime uscita2, String note, LocalDate data) {
		this.user = user;
		this.entrata1 = entrata1;
		this.uscita1 = uscita1;
		this.entrata2 = entrata2;
		this.uscita2 = uscita2;
		this.note = note;
		this.data = data;
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public String getUser() {
		return user;
	}





	public void setUser(String user) {
		this.user = user;
	}





	public LocalTime getEntrata1() {
		return entrata1;
	}





	public void setEntrata1(LocalTime localTime) {
		this.entrata1 = localTime;
	}





	public LocalTime getUscita1() {
		return uscita1;
	}





	public void setUscita1(LocalTime localTime) {
		this.uscita1 = localTime;
	}





	public LocalTime getEntrata2() {
		return entrata2;
	}





	public void setEntrata2(LocalTime localTime) {
		this.entrata2 = localTime;
	}





	public LocalTime getUscita2() {
		return uscita2;
	}





	public void setUscita2(LocalTime localTime) {
		this.uscita2 = localTime;
	}





	public String getNote() {
		return note;
	}





	public void setNote(String note) {
		this.note = note;
	}





	public LocalDate getData() {
		return data;
	}





	public void setData(LocalDate localDate) {
		this.data = localDate;
	}
	
	
	
	
}
