package it.f2Informatica.springsecurity.demo.controller;


import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import it.f2Informatica.springsecurity.demo.config.UserDao;
import it.f2Informatica.springsecurity.demo.entity.User;





@Controller
public class DemoController {
	
	@Autowired
    private UserDao userDao;
 
	
	@GetMapping("/")
	public String showHome() {
		return "home"; //ritorna la pagina home.jsp
	}
	
	
	//add request mapping for leader
	@GetMapping("/leaders")
	public String showLeaders() {
		return "leaders"; //ritorna la pagina leader.jsp
	}
	
	
	//add request mapping for system
		@RequestMapping("/systems")
		public String showListUser(Model model) {
			
			 // get User from the service
			  List<User> listUser = userDao.list();
			  
			  // add users at the model
			  model.addAttribute("listUser", listUser);
			  
			  //ritorna la pagina systems.jsp
			return "systems";
		}
		
	
	
		
		//crea il mapping per inserire o aggiornare un nuovo utente
		@GetMapping("/showFormAdd")
		public String showFormAdd(Model model) {
			//create model attribute for new user
			User user= new User();
			model.addAttribute("user", user);  //"user" � il valore che si trova in modelAttributr in userForm
			
			return "userForm";
		}
		
		//richiama la Dao per momorizzare l'utente
		@PostMapping("/saveUser")
		public String saveUser(@ModelAttribute("user") User user) {
			if (user.getUsername()== null) {
			userDao.saveUser(user);
			return"redirect:/systems"; 
		}else 
			return "access-denied";
		}
		
		
		//crea il mapping per la modifica password
		@RequestMapping("/modificaPass")
		public String modificaPass(Model model) {
			//create model attribute for new user
			User user= new User();
			model.addAttribute("user", user);  //"user" � il valore che si trova in modelAttributr in modificaPass
			
			return "modifica-pass";
		}
		
		
		//richiama la dao per la modifica password
		@PostMapping("/updatePass")
		public String updatePass(@ModelAttribute("user") User user) {
			userDao.updatePass(user);
			return"returnPass"; 
		}
		
		@RequestMapping("/eliminaUser")
		public String eliminaUser(Model model) {
			//create model attribute for new user
			User user= new User();
			model.addAttribute("user", user);  //"user" � il valore che si trova in modelAttributr in eliminaUser
			
			return "eliminaUser";
		}
		
		@RequestMapping(value = "/deleteUser")
		public String deleteUser(@ModelAttribute("user") User user) {
		  
		    userDao.deleteUser(user);
		    return "redirect:/systems" ;
		}
		
		
				
		// crea il mapping per la pagina di access-denied
				@GetMapping("/access-denied")
				public String showAccessDenied() {
					
					return "access-denied";
					
				}
				
				
				
				
				
			/*	@RequestMapping(value = "/newContact", method = RequestMethod.GET)
				public ModelAndView newContact(ModelAndView model) {
				    User newUser = new User();
				    model.addObject("user", newUser);
				    model.setViewName("userForm");
				    return model;
				}
				*/
				
			/*	@GetMapping("/showFormUpdate")
				public String showFormUpdate(@RequestParam("userName") String use, Model model) {
					
					// get the user from userDao
					userDao.getUser(use);
					
					//create model attribute for new user
					User user= new User();
					
					// set user as a model attribute to pre populate the form
					model.addAttribute("user", user);
					
					return "userForm";
					
				}*/
				
			/*	@RequestMapping(value = "/saveContact", method = RequestMethod.POST)
				public ModelAndView saveContact(@ModelAttribute User user) {
				    userDao.saveOrUpdate(user);
				    return new ModelAndView("redirect:/systems");
				}*/
				
				/*	@RequestMapping(value="/systems")
				public ModelAndView listContact(ModelAndView model) throws IOException{
				    List<User> listUser = userDao.list();
				    model.addObject("listUser", listUser);
				    model.setViewName("systems");
				 
				    return model;
				}*/
				
				
}
