package it.f2Informatica.springsecurity.demo.controller;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import it.f2Informatica.springsecurity.demo.config.PresenzeDao;
import it.f2Informatica.springsecurity.demo.entity.Presenze;



@Controller
public class TimbraController {
	
	@Autowired
    private PresenzeDao presenzeDao;
 
	
	@RequestMapping(value="/timbra", method= RequestMethod.GET)
	@ResponseStatus(value=HttpStatus.OK)
	public String timbra(@ModelAttribute Presenze presenze) {
		
		
		// recupera l'entit� corrente autenticata con una chiamata a SecurityContextHolder
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		System.out.println(currentPrincipalName);
		
		// memorizza in una variabile l'utente corrente
		String name= currentPrincipalName;
		
		//query di ricerca per username da presenze
				
		Presenze p= presenzeDao.getPresenze(LocalDate.now(),name);
			
			
		//inserire i controlli per la scrittura
		
		if (p == null && LocalTime.now().getHour() >=9) {
			
			p = new Presenze();
			p.setData(LocalDate.now());
			p.setEntrata1(LocalTime.now());
			p.setUser(name);
			presenzeDao.insertPresenze(p);
			
		}else if (p != null && p.getUscita1() == null) {
				p.setUscita1(LocalTime.now());
				presenzeDao.updatePresenze(p);
				
			}else if(p != null && p.getEntrata1() == null) {
					p.setEntrata2(LocalTime.now());
					presenzeDao.updatePresenze(p);
					
				}else if(p != null && p.getUscita2() == null) {
						p.setUscita2(LocalTime.now());
						presenzeDao.updatePresenze(p);
					}
			
		
		
		return "redirect:/timbratura";
	}

}
