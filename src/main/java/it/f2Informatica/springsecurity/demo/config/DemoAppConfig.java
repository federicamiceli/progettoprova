package it.f2Informatica.springsecurity.demo.config;

import java.beans.PropertyVetoException;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.mchange.v2.c3p0.ComboPooledDataSource;


@Configuration
@EnableWebMvc
@ComponentScan(basePackages="it.f2Informatica.springsecurity.demo")
@PropertySource("classpath:persistence-mysql.properties")
public class DemoAppConfig {
	
	//set up variable per contenere le propriet� di connessione poste nel file di propriet�
	@Autowired
	private Environment env;
	
	// set up logger per la diagnostica
	private Logger logger= Logger.getLogger(getClass().getName());
	
	
	//define a Bean for viewRisolver
	@Bean
	public ViewResolver ViewResolver() {
		InternalResourceViewResolver viewResolver= new InternalResourceViewResolver();
		viewResolver.setPrefix("/WEB-INF/view/");		//ViewResolver ci consente di impostare propriet� come prefisso o suffisso al nome della vista 
		viewResolver.setSuffix(".jsp");					//per generare l'URL della pagina di visualizzazione finale
		return viewResolver;
	}
	
	//configuro un bean per il security datasource
	 @Bean
	 public DataSource SecurityDatasource() {
		 
		 //creo il pool di connessione
		 ComboPooledDataSource securityDataSource = new ComboPooledDataSource();
		 	 
		 //set the jdbc driver class
			 try {
				securityDataSource.setDriverClass(env.getProperty("jdbc.driver"));
			} catch (PropertyVetoException exc) {
				throw new RuntimeException(exc);
				
			}
		 
		 //registro the connection property per vedere se sono state memorizzate correttamente
		  logger.info(">>> jdbc.url=" + env.getProperty("jdbc.url"));	 
		  logger.info(">>> jdbc.user=" + env.getProperty("jdbc.user")); 
		  
		 //set database connection property
		  securityDataSource.setJdbcUrl(env.getProperty("jdbc.url"));
		  securityDataSource.setUser(env.getProperty("jdbc.user"));
		  securityDataSource.setPassword(env.getProperty("jdbc.password"));
		  
		  //set connection pool property
		  securityDataSource.setInitialPoolSize(
				  getIntProperty("connection.pool.initialPoolSize"));
		  securityDataSource.setMinPoolSize(
				  getIntProperty("connection.pool.minPoolSize"));
		  securityDataSource.setMaxPoolSize(
				  getIntProperty("connection.pool.maxPoolSize"));
		  securityDataSource.setMaxIdleTime(
				  getIntProperty("connection.pool.maxIdleTime"));
		 
		 return securityDataSource;
		 
		 
	 }
	 //metodo di supporto che converte le propriet� passate da stringa in int
	 
	 private int getIntProperty(String propName) {
		 String propVal= env.getProperty(propName);
		 // now convert to int
		 int intProVal = Integer.parseInt(propVal);
		 return intProVal;
	 }
	
	 // getUserDao restituisce un'implementazione dell'interfaccia UserDao , che � la classe UserDaoImpl
	 // Si noti che il metodo SecurityDatasource() restituisce il bean Datasource configurato
	 @Bean
	    public UserDao getUserDao() {
	        return new UserDaoImpl(SecurityDatasource());
	    }
	 
	 @Bean
	    public PresenzeDao getPresenzeDao() {
	        return new PresenzeDaoImpl(SecurityDatasource());
	    }
}
