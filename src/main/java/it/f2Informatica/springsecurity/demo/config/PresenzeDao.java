package it.f2Informatica.springsecurity.demo.config;
import java.time.LocalDate;
import java.util.List;

import it.f2Informatica.springsecurity.demo.entity.Presenze;


public interface PresenzeDao {
	
	
	 //public List<Presenze> list();
	 public void updatePresenze(Presenze presenze);
	 public Presenze getPresenze(LocalDate data, String username);
	 public void insertPresenze(Presenze presenze);
}
