package it.f2Informatica.springsecurity.demo.config;


import java.util.List;

import it.f2Informatica.springsecurity.demo.entity.User;

public interface UserDao {
	
	
	 public void saveUser(User user);
	 
		 
	    public void deleteUser(User user);
	     
	    //public Contact get(int contactId);
	     
	 public List<User> list();

	//public User getUser(String use);
	
	public void updatePass(User user);
}
