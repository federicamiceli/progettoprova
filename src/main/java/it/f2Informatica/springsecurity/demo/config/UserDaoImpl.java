package it.f2Informatica.springsecurity.demo.config;




import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import it.f2Informatica.springsecurity.demo.entity.User;



public class UserDaoImpl implements UserDao {
	
	private JdbcTemplate jdbcTemplate;
	
	 public UserDaoImpl(DataSource dataSource) {
	        jdbcTemplate = new JdbcTemplate(dataSource);
	    }
	 
	//inserisce un nuovo utente
	@Override
	public void saveUser(User user) {
			String pass= "{noop}"  + user.getPass();
		    // insert
	        String sql = "INSERT INTO users (username, password, enabled)"
	                    + " VALUES (?, ?, ?)";
	        jdbcTemplate.update(sql, user.getUsername(), pass, user.getEnabled());
	         //insert roles
	        String name= user.getUsername();
	        String role= user.getAuthorities();
	        String sql2 = "INSERT INTO authorities (username, authority) VALUE ('"+name+"', '"+role+"')";
	        //jdbcTemplate.update(sql2, name, user.getAuthorities());
	        jdbcTemplate.update(sql2);
	    }
	 
		
	//modifica la password utente
	@Override
	public void updatePass(User user) {

	      	String password= "{noop}"  + user.getPass();
	        String sql = "UPDATE users SET password= '"+password+"' WHERE username= ?";
	        jdbcTemplate.update(sql, user.getUsername());
		
	}
	

	//stampa la lista utenti
	public List<User> list() {
	    String sql = "SELECT * FROM `users`";
	    
	    List<User> listUser = jdbcTemplate.query(sql, new RowMapper<User>() {
	 
	     @Override
	        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
	            User aUser = new User();
	 
	            aUser.setUsername(rs.getString("username"));
	            aUser.setPass(rs.getString("password"));
	            aUser.setEnabled(rs.getInt("enabled"));
	            
	            return aUser;
	        }
	 
	    });
	 	        
	    return listUser;
	}
	
	
	public void deleteUser(User user) {
	    String sql = "DELETE FROM users WHERE username=?";
	    jdbcTemplate.update(sql, user.getUsername());
	}


	
	
	
	}
