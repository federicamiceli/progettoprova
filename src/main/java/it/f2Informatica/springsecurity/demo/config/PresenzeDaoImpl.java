package it.f2Informatica.springsecurity.demo.config;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import it.f2Informatica.springsecurity.demo.entity.Presenze;




public class PresenzeDaoImpl implements PresenzeDao {

	private JdbcTemplate jdbcTemplate;
	
	 public PresenzeDaoImpl(DataSource dataSource) {
	        jdbcTemplate = new JdbcTemplate(dataSource);
	    }
	 
	 
	 
	 
//	 //stampa la lista degli utenti selezionati con i relativi campi
//	 public List<Presenze> list() {
//		 Presenze presenze= new Presenze();
//		 
//		 String name= presenze.getUser();
//		 
//	 String sql= "SELECT * FROM presenze WHERE 	id_username= '"+name+"' ORDER BY id_username;";
//		 
//		 List<Presenze> listPresenze = jdbcTemplate.query(sql, new RowMapper<Presenze>() {
//			 
//		     @Override
//		        public Presenze mapRow(ResultSet rs, int rowNum) throws SQLException {
//		            Presenze presenze = new Presenze();
//		 
//		            presenze.setUser(rs.getString("username"));
//		            presenze.setEntrata1(rs.getTime("entrata1"));
//		            presenze.setUscita1(rs.getTime("uscita1"));
//		            presenze.setEntrata2(rs.getTime("entrata2"));
//		            presenze.setUscita2(rs.getTime("uscita2"));
//		            presenze.setNote(rs.getString("note"));
//		            presenze.setDate(rs.getDate("data"));
//		            
//		            return presenze;
//		        }
//		 
//		    });
//		 	return listPresenze;
//	 }

	 @Override
		public void insertPresenze(Presenze presenze) {
		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String name = authentication.getName();
			
			System.out.println(presenze.getId());
		
		 String sql = "INSERT INTO presenze (username, entrata1,  data) VALUES (?, ?, ?)";
				 jdbcTemplate.update(sql, name, presenze.getEntrata1(), presenze.getData());

	 }
	 
	 
	 
	 @Override
		public void updatePresenze(Presenze presenze) {
		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			String name = authentication.getName();
			String utente= presenze.getUser();
			
				
				 String sql = "UPDATE presenze SET entrata1=? , uscita1=?, entrata2=?, uscita2=? ";
			        jdbcTemplate.update(sql, presenze.getEntrata1(), presenze.getUscita1(), 
		        					presenze.getEntrata2(), presenze.getUscita2());
	 }
	 
	 
	 
	 

	 public Presenze getPresenze(LocalDate data, String username) {
//		 Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//			String name = authentication.getName(); 
			
		 String sql= "SELECT data, username FROM presenze WHERE username= '" +username+"' AND data= "+data;
		 
		return jdbcTemplate.query(sql, new ResultSetExtractor<Presenze>() {
			 
		        @Override
		        public Presenze extractData(ResultSet rs) throws SQLException,
		                DataAccessException {
		        	Presenze presenze = new Presenze();
		            if (rs.next()) {
		                
		                presenze.setUser(rs.getString("username"));
		                
		                return presenze;
		            }
		 
		            return null;
		        }
		 
		    });
		}
		            
}
